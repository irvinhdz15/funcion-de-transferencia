from flask import Flask, request
from flask_restful import Resource, Api
from json import dumps
from flask_jsonpify import jsonify
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from flask import Flask
from flask_cors import CORS


app = Flask(__name__)
api = Api(app)
CORS(app)


@app.route("/ft",methods=['GET'])
def ft ():

     tau=float(request.args.get('tau')) # time constant TIEMPO EN X (1/TAO)
     z=float(request.args.get('z')) # damping factor AMORTIGUAMIENTO

     Kp = 1.0    # gain DONDE SE ESTABILIZA LA GRAFICA
     du = 1.0    # change in u CAMBIA ESCALA EN Y

     # (1) Transfer Function
     num = [Kp]
     den = [tau**2,2*z*tau,1]
     sys1 = signal.TransferFunction(num,den)
     t1,y1 = signal.step(sys1)
     return jsonify(t1=list(t1),y1=list(y1))

def add_cors_headers_response_callback(event):
     def cors_headers(request, response):
          response.headers.update({
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
          'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, Authorization',
          'Access-Control-Allow-Credentials': 'true',
          'Access-Control-Max-Age': '1728000',
          })
     event.request.add_response_callback(cors_headers)



if __name__ == '__main__':
     app.run(port='8000')

